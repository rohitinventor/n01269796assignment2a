﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" CodeBehind="database.aspx.cs" Inherits="assignment2a.database" %>
<asp:Content id="body" ContentPlaceHolderID="basicConcept" runat="server">
    
   
   
             
         
                <h2>About Database</h2>
                <p>Database is used to store the client information like Usernsame,Passward, and useful data.
                    In our course we have learnt about CRUD operations.I feel really helpful in my programming.  
                </p>
                
     </asp:Content>
        
      
 


<asp:Content ContentPlaceHolderID="mySnippetCode" runat="server">        
      <h2>My Code for the assignment</h2>
                <pre>
                  SELECT * 
                  FROM INVOICES
                </pre>
                <p>This will select all the columns in the table invoices and display it. </p>
    </asp:Content>

    
    <asp:Content ContentPlaceHolderID="referenceSnippetCode" runat="server">        
     <h2>Database Example</h2>
                <pre>
                    SELECT LastName, FirstName, Address FROM Persons
                    WHERE Address IS NULL 
                </pre>
                
                <p>This will select and display lastname,firstname nad adress from persons table where adress is null.I have taken this code from W3school.</p>
                
    </asp:Content>

        
        
        
        <asp:Content ContentPlaceHolderID="link" runat="server">        
          <h2>List of helpful links</h2>
               <ul>
                    <li><a href ="https://www.w3schools.com/sql/sql_null_values.asp">W 3 SCHOOL</a></li>
                     <li><a href="https://www.ucl.ac.uk/archaeology/cisp/database/manual/node1.html">UCL</a></li>
                     <li><a href="https://en.wikipedia.org/wiki/Database">Wikipedia</a></li>
                     
                </ul>
    
    </asp:Content>
