﻿<%@ Page Language="C#"  MasterPageFile="~/Site.Master" CodeBehind="javascript.aspx.cs"  Inherits="assignment2a.javascript" %>

<asp:Content id="body" ContentPlaceHolderID="basicConcept" runat="server">
    
    
    <h2>About JavaScript</h2>
                <p>
                In this course, i have learnt about datatypes,array,funtions,object. Creating project using these elements is very intresting and 
                i used to add some creativity for the sam.    

                </p>
</asp:Content>
      




<asp:Content ContentPlaceHolderID="mySnippetCode" runat="server"> 
      
                <h2>My Code for the assignment</h2>
              
                <p>
                JavaScript arrays are used to store multiple values in a single variable and we can sort them.
                <br>Examples
                </p>
                
<pre>
var fruits = ["Banana", "Orange", "Apple", "Mango"]; 
(1)  function sort(){
fruits.sort();   
document.getElementById("result").innerHTML = "Array : "+fruits;
}
(2) function reverse(){
fruits.reverse();  
document.getElementById("result").innerHTML = "Array : "+fruits;
}

</pre>

                     <p id="result"></p>
                     <script type="text/javascript">
                        
                        var fruits = ["Banana", "Orange", "Apple", "Mango"]; 
                        document.getElementById("result").innerHTML = "Array : "+fruits;
                        
                        function sortArray(){
                         fruits.sort();  
                      
                        document.getElementById("result").innerHTML = "Array : "+fruits;

                        }
                        
                        function reverseArray(){
                        fruits.reverse();  

                        document.getElementById("result").innerHTML = "Array : "+fruits;

                            }
                    </script>
               
                <button id="sortbutton"  type="button" onclick="sortArray();">Sort The Array</button>
                <button id="reversebutton" type="button" onclick="reverseArray()">Reverse the array</button>
</asp:Content>
    
    
<asp:Content ContentPlaceHolderID="referenceSnippetCode" runat="server">        
                <h2>JavaScript Example</h2>
                  <p>This code will add two numbers using function add().I have taken this code from W3SCHOOL</p>

    <pre>
function add(){
var price1 = 5;
var price2 = 6;
var total = price1 + price2;  
document.getElementById("resultTotal").innerHTML = "Result : "+total;
}</pre>
                <p id="resultTotal" ></p>
                <script>
                function add(){
                   var price1 = 5;
                    var price2 = 6;
                    var total = price1 + price2;  
                    document.getElementById("resultTotal").innerHTML = "Result : "+total;

                    }
                    
                </script>
                <button id="addButton"  type="button" onclick="add();">Try it</button>
      
                
</asp:Content>
    


<asp:Content ContentPlaceHolderID="link" runat="server"> 
       <h2>List of helpful links</h2>
              
        <ul>
                    <li><a href="https://www.w3schools.com/js/default.asp">W 3 SCHOOL</a></li>
                     <li><a href="https://www.javascript.com/">Javascript.com</a></li>
                     <li><a href="https://en.wikipedia.org/wiki/JavaScript">Wikipedia</a></li>
                     
                </ul>
                
</asp:Content>